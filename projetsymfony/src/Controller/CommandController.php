<?php

namespace App\Controller;

use App\Entity\Command;
use App\Entity\Product;
use App\Repository\CommandRepository;
use App\Repository\ProductRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class CommandController extends AbstractController
{
    /**
     * @Route("/command", name="command")
     */
    public function index(ProductRepository $productRepository, CommandRepository $commandRepository, PaginatorInterface $paginator, Request $request)
    {
        $commandRepository = $this->getDoctrine()->getRepository(Command::class);
        $productRepository = $this->getDoctrine()->getRepository(Product::class);
        $command = $commandRepository->findAll();
        return $this->render('command/index.html.twig', [
            'listCommand' => $command,
        ]);
    }

    /**
     * @Route("/command/{id}", name="command.show")
     */
    public function show($id, CommandRepository $commandRepository){
        
        $lacommand = $commandRepository->find($id);
        $listproduit = [];
        $prixtotal = 0;
        $nbTotalProdcut = $lacommand->getNbProduct();
        foreach($lacommand->getProducts() as $unproduit){
            array_push($listproduit, $unproduit);
            $prixtotal = $prixtotal + $unproduit->getPrice();
        }

        return $this->render('command/show.html.twig', [
            'laCommand' => $lacommand,
            'listProduit' => $listproduit,
            'prixTotal' => $prixtotal,
            'nbProduct' => $nbTotalProdcut,
        ]);
    }
}
