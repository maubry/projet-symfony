<?php

namespace App\Controller;

use App\Entity\Command;
use App\Entity\Product;
use App\Form\CommandType;
use App\Repository\ProductRepository;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class PanierController extends AbstractController
{
    /**
     * @Route("/panier", name="panier")
     */
    public function index(SessionInterface $session, ObjectManager $manager, Request $request, ProductRepository $productRepository)
    {
        $lepanier = new Command();
        $panier = $session->get('panier', []);

        $manager = $this->getDoctrine()->getManager();
        
        
        $panierForm = $this->createForm(CommandType::class, $lepanier);
        $panierForm->handleRequest($request);

        
        $totalprix = 0;
        foreach ($panier as $produit) {
            $totalprix = $totalprix + $produit->getPrice();
        }

        if ($panierForm->isSubmitted() && $panierForm->isValid()) {
            if(sizeof($panier) > 0){
                $lepanier->setCreatedAt(new \DateTime);
                foreach ($panier as $id => $quantity) {
                    $lepanier->addProduct($productRepository->find($id));
                }
                $manager->persist($lepanier);
                $manager->flush();
                $this->addFlash('success', "La commande a bien été passée.");
                $session->set('panier', []);
                return $this->redirectToRoute('panier');
            }else{
            $this->addFlash('danger', "Erreur lors de la commande");
            }   
        }

        return $this->render('panier/index.html.twig', [
            'listPanier' => $panier,
            'totalPrix' => $totalprix,
            'panierForm' => $panierForm->createView(),
        ]);
    }

    /**
     * @Route("/panier/add/{id}", name="panier.add")
     */
    public function add($id, SessionInterface $session, Product $produit, ProductRepository $productRepository)
    {
        if($produit){
            $panier = $session->get('panier', []);
            if($panier){
                $panier[$id] = $productRepository->find($id);
            }else{
                $panier = array();
                $panier[$id] = $productRepository->find($id);
            }
            $session->set('panier', $panier);
            $this->addFlash('success', "Le produit {$produit->getName()} a bien été ajouté au panier !");
            return $this->json('ok', 200);
        }else{
            return $this->json('nok', 404);
        }
    }

    /**
     * @Route("/panier/delete/{id}", name="panier.delete")
     */
    public function delete(Request $request, $id, SessionInterface $session, Product $produit, ProductRepository $productRepository)
    {
        if (!$produit) {
            throw $this->createNotFoundException('Le produit n\'existe pas');
        }

        $csrfToken = $request->request->get('token');{

        if ($this->isCsrfTokenValid('delete-item', $csrfToken)) 
            if($produit){
                $panier = $session->get('panier', []);
                if($panier){
                    unset($panier[$id]);
                    $this->addFlash('success', "Le produit {$produit->getName()} a bien été supprimé au panier !");
                }
                $session->set('panier', $panier);
            }
        }
        return $this->redirectToRoute('panier');
    }
}
