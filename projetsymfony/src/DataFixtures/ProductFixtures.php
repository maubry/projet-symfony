<?php

namespace App\DataFixtures;

use Faker\Factory;
use App\Entity\Product;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class ProductFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();

        for ($i = 1; $i <= 20; $i++) {
            $product = new Product();

            $sentence = $faker->sentence(4);
            $name = substr($sentence, 0, strlen($sentence) - 1);

            $product->setName($name)
                    ->setDescription($faker->text(1500))
                    ->setCreatedAt($faker->dateTimeThisYear())
                    ->setPrice($faker->randomNumber(2));

            $manager->persist($product);
        }

        $manager->flush();
    }
}
